# Ajouter une base mongodb et compléter le code python de l'API

## Introduction

Dans le TP précédent, nous avons mis en place notre serveur et le squelette de
notre programme. Dans ce TP, nous allons programmer nos opérations et utiliser
une base de donnée afin d’y stocker les informations que notre API
permet de gérer.

### Pré-requis

- Avoir réalisé les TP1 et TP2
- Disposer de putty et winscp sur son PC, savoir s’en servir, pour se
  connecter à la VM et y télécharger des fichiers à partir de son PC.
- Savoir éditer/modifier un fichier en mode console sur Linux

Ce TP comporte 3 étapes:

1. J’installe la base de données et le nécessaire pour que mes programmes
  python puissent s’en servir
1. Je modifie le code de mes opérations
1. Je déploie mon code sur la VM, je teste

***

## Etape 1: Installation de la base de donnée

Objectif: J’installe la base de données et le nécessaire pour que mes
programmes python puissent s’en servir

Se connecter à la VM avec Putty

Installation de mongodb:

```
sudo apt-get update
sudo apt-get install mongodb
```

pour que mon programme python puisse utiliser cette base de donnée, il
faut un autre programme: PyMongo

installation de PyMongo:

```
python3 -m pip install pymongo
```

## Etape 2: Je modifie le code de mes opérations

Nous allons compléter le code décrit dans le fichier `book_controller.py`
(voir TP N°2)

Code initial:

```
def book_create(book) -> str:
  return 'do some magic!'

def book_delete(bookId) -> str:
  return 'do some magic!'

def book_list() -> str:
  return 'do some magic!'

def book_patch(bookId, book) -> str:
  return 'do some magic!'

def book_update(bookId, book) -> str:
  return 'do some magic!'

def bookfind_by_id(bookId) -> str:
  return 'do some magic!'
```

Nouveau code: voir fichier ci-joint.

Prenez le temps de regarder son contenu.
Les commentaires devraient vous permettre de comprendre le code.

## Etape 3: Je déploye mon code sur la VM, je teste

Il vous faut remplacer le fichier `book_controller.py` par le fichier
ci-joint.

WinSCP est le logiciel à utiliser pour transférer des fichiers entre votre PC
windows et votre VM

Sur votre VM, pour rappel (TP2), le fichier à remplacer est dans le
répertoire: `/home/ubuntu/myAPIs/myLibrary/controllers`

Une fois le fichier `book_controller.py` remplacé, nous allons activer
notre serveur d’API

```
cd /home/ubuntu/myAPIs/myLibrary/python3 app.py
```

Puis nous allons utiliser manuellement notre API, cela devrait faire
apparaître la ligne suivante:
`* Running on http://0.0.0.0:8080/(Press CTRL+C to quit)`

Sur son PC, ouvrir son navigateur Web à l’adresse
suivante: `@ip-flottante-de-la-VM:8080/api/V1/ui/`

L’écran suivant apparait:

![img menu swagger](./img/img3/swagger.png)

Et maintenant, les opérations sont réellement implémentées pour
créer/modifier/supprimer/consulter les livres dans la base de données.

![img post](./img/img3/post.png)

Fin du TP/Tuto N°3
