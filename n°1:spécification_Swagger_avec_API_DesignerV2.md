# Spécification Swagger avec API Designer V2

Durée de ce TP/tuto : 20 minutes environ

## Objectifs

- J’apprends à faire la spécification d’une API Rest simple en utilisant
  l’outil API Designer V2 d’Orange
- J’obtiens une documentation «Swagger», le standard du domaine.

Les 7 étapes:

1. Se créer un projet sur API Designer
1. Création d’une API au sein du projet
1. Éditer l’API
1. Déclarer les objets (modèle)
1. Déclarer les opérations
1. Configurer son «environnement»
1. Apprécier le résultat

***

Outils: PC avec Firefox (éviter Internet Explorer) et accès à
[API Designer V2 d’Orange](https://api-designer.sso.infra.ftgroup/ui/#/ad/home)

## Étape 1: se créer un projet sur API Designer

Taper l’URL de API Designer, vous devez arriver sur la page suivante:

![img accueil](./img/img1/accueil.png)

Il nous faut créer un projet: il s’agit d’une notion administrative qui peut
contenir plusieurs API avec un certain nombre de personnes ayant le droit de
consultation/modifications.

- Cliquer sur «add project»
- Remplir le champ «name» par «Library» (ou tout autre choix...) et une petite
  description.

On obtient:

![img liste des projets](./img/img1/listeProjet.png)

## Étape 2: création d’une API au sein du projet

On clique sur le projet que l’on vient de créer: pour moi c’était «Library».
Un écran avec la liste de toutes vos spécifications d’API apparaît
(sans doute vide pour vous)

L’écran de création d’API au sein du projet «Library» apparaît:

![img project home](./img/img1/home.png)

Remplir le champ «name», indiquer une version, choisir son outil d’édition:
API Designer ou Swagger Editor
(le tutoriel est basé sur l’utilisation d’API Designer)

![img project init](./img/img1/initialisation.png)

cliquer sur *add* en bas => l’API est désormais initialisée.

## Étape 3: Éditer l’API

Cliquer sur le symbole qui représente un crayon pour ouvrir l’écran d’édition
de l’API

![img menu projet](./img/img1/project_menu.png)

Ecran de saisie de l’API apparait:

![img ecran de saisie](./img/img1/ecran_de_saisie.png)

## Étape 4: Déclarer les objets (modèle)

Cliquer sur le bouton *add* à côté de «classes»

![img ajouter objet](./img/img1/ecran_de_saisie_fleche.png)

- Saisir «book» et appuyer sur «add class»
- Ajouter un premier attribut «id»
- Choisir son type, décider s’il est obligatoire, s’il peut y en avoir
  plusieurs (array), et cliquer sur le + en fin de ligne pour valider.

Répéter pour chaque attribut: author, title, price

![img classe book](./img/img1/book.png)

Pour l’attribut Category, il s’agit d’un type ENUM (un choix dans une liste de
  valeurs pré-définies).
Au moment de la sélection du type, il est possible de faire un «add enum»et de
donner un nom à cet ENUM (BookCategory) et de renseigner*
les valeurs possibles.

Avec l’idée qu’un livre pourrait appartenir à plusieurs catégories,
on coche la case «array».

![img classe category](./img/img1/category.png)

On peut remarquer que l’ENUM «BookCategory» est désormais accessible pour
modifications ultérieures dans la partie gauche de l’éditeur.

Pour modifier un nom, type, cardinalité déjà saisi ... il suffit de cliquer
sur ce que l’on veut modifier

## Étape 5: Déclarer les opérations sur notre livre

Cliquer sur l’onglet «operations»

![img menu](./img/img1/vers_operations.png)

- Cliquer sur le symbole + à côté de «ressources»
- Dans le menu déroulant «class», sélectionner la classe d’objet «book» sur
  laquelle porteront les opérations de notre API
- Cliquer sur «add ressource» et vous devez obtenir l’écran suivant:

![img menu ressources](./img/img1/ressources.png)

Sélectionner les opérations *Create*, *Update*, *Delete*, *Find*, *Get*,
*Patch* en déplaçant le bouton curseur sur chaque opération Rest que vous
souhaitez

## Étape 6: onglet «endpoints»

Il s’agit d’indiquer l’url du serveur sur lequel se trouvera l’API.
Il peut y avoir plusieurs serveurs.

Nous allons déclarer un faux serveur appelé `www.example.com`

![img endpoints](./img/img1/endpoints.png)

Cela permettra d’avoir une jolie url avec la version de votre API (conformément
  aux recos d’Orange sur le design des API Rest : [lien vers l'article
  ici](http://recommendations.si.fr.intraorange/designing-orange-api/))

![img exemple serveur](./img/img1/serveurBidon.png)

## Étape 7: voir le résultat

C’est fini!

Vous avez spécifié l’essentiel de votre API Rest.
Vous pouvez visualiser le résultat de votre spécification selon deux modes:

- **Mode graphique**: cliquer sur le symbole vert de swagger

![img lien vers le swagger](./img/img1/lien.png)

Un nouvel onglet s’ouvre dans votre navigateur web

![img swagger](./img/img1/swagger.png)

Cliquer sur *book*, puis sur *GET*, puis *Try it out!*

Évidemment, ce n’est pas opérationnel : vous n’avez réalisé que la «spec»
(il faudra un autre TP pour réaliser le code exécutable!)

- **Mode texte JSON**:

- Copier l’url qui se trouve sur la ligne verte swagger:
- Ouvrez un nouvel onglet dans votre navigateur web
- Coller cette URL dans le champ d’adresse de site et valider

Vous obtenez la visualisation de votre spécification d’API en JSON conforme au
standard «OPEN API» (anciennement appelé «swagger»).

![img json](./img/img1/json.png)
